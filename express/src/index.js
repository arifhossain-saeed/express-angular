const express = require('express');


const App = express();
// App.use(router);

App.get('/', function(req, res){
    return res.send("Hello World. From Faria and Saeed.");
});

let coderName = 'Faria';

App.get('/saeed', (req, res) => {
    const coder = {
        name: 'Saeed',
        age: 28,
        designation: 'Vadaimma'
    }
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.status(200).json({msg: `This code is from ${coderName}`, data: coder});
})

const PORT = 5001;

App.listen(PORT, ()=> {
    console.log(`Server is running at port ${PORT}`);
})




