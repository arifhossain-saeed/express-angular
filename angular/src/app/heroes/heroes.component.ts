import { Component, OnInit } from '@angular/core';

import { Hero } from '../hero';

import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';

interface UserData {
  name: string,
  age: number,
  designation: string
}

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  data = {};

  hero: Hero = {
    id: 1,
    name: 'Windstorm'
  };

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.data = this.http.get('http://localhost:5001');
    console.log(this.data);
  }

}
