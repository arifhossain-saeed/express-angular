import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {

  gotData: any = {};

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    // this.dataService.getData().subscribe((res) => this.gotData = res.data)
  }

  getDataOnClick(): void{
    this.dataService.getData().subscribe((res) => this.gotData = res.data)
    // console.log(this.gotData.data);
  }

}
