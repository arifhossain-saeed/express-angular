import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Data } from '../types/Data';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private apiUrl = "http://localhost:5001/saeed";

  constructor(private http:HttpClient) { }

  getData(): Observable<any>{
    return this.http.get<any>(this.apiUrl);
  }
}
