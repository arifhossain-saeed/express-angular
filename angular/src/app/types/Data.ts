export interface Data {
    name: string,
    age: string | number,
    designation: string
}